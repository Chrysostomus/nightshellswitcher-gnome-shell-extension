# Night Shell Switcher

![](./screenshot.png)

Automatically change the GNOME shell theme to dark variant when Night Light activates.

**Important**: You must have enabled the [User Themes extension](https://extensions.gnome.org/extension/19/user-themes/).

_Do you need to change your GTK theme as well? Try [Night Theme Switcher](https://gitlab.com/rmnvgr/nightthemeswitcher-gnome-shell-extension/)!_

## Theme compatibility

Your shell theme must have a `-dark` variant, e.g. `Yaru` and `Yaru-dark`.

These shell themes have been tested and work:

- Arc
- Materia
- Yaru

Let me know if the theme you use works as well, or if it doesn't, I can try to make it work.

## Graphical installation

Visit [the extension page on extensions.gnome.org](https://extensions.gnome.org/extension/2356/night-shell-switcher/) and enable the extension.

## Command line installation

Clone the repository and enter the directory:

```bash
git clone https://gitlab.com/rmnvgr/nightshellswitcher-gnome-shell-extension.git && cd nightshellswitcher-gnome-shell-extension
```

Install using `make`:

```bash
make install
```

Restart your GNOME session and enable the extension:

```bash
gnome-extensions enable nightshellswitcher@romainvigier.fr
```
